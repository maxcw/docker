AnalysisBase/AnalysisTop-21.2.X Image Configuration
===================================================

This configuration can be used to build an image providing a
completely standalone installation of an
AnalysisBase/AnalysisTop-21.2.X release.

Building an image for the latest AnalysisBase release should be done
with:

```bash
docker build -t atlas/analysisbase:21.2.12-20171208 \
   -t atlas/analysisbase:21.2.12 -t atlas/analysisbase:latest \
   --build-arg RELEASE=21.2.2 --compress --squash .
```

For AnalysisTop you would use this formalism:

```bash
docker build -t atlas/analysistop:21.2.12-20171208 \
   -t atlas/analysistop:21.2.12 -t atlas/analysistop:latest \
   --build-arg PROJECT=AnalysisTop --build-arg RELEASE=21.2.2 \
   --compress --squash .
```

You should of course just leave off the "latest tag" from the command
line when building an image for an older release.

Examples
--------

You can find pre-built images on
[atlas/analysisbase](https://hub.docker.com/r/atlas/analysisbase/) and
[atlas/analysistop](https://hub.docker.com/r/atlas/analysistop/).
